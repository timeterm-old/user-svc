module gitlab.com/timeterm/user-svc

go 1.13

replace git.apache.org/thrift.git => github.com/apache/thrift v0.0.0-20180902110319-2566ecd5d999 // indirect

require (
	github.com/francoispqt/gojay v1.2.13
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/gogo/protobuf v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/rs/zerolog v1.15.0
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/stretchr/testify v1.4.0 // indirect
	gitlab.com/timeterm/env v0.0.0-20191001061817-01717c815dce
	gitlab.com/timeterm/proto/go v0.0.0-20191008102224-95aa5b974edb
	golang.org/x/net v0.0.0-20191007182048-72f939374954 // indirect
	golang.org/x/sys v0.0.0-20191008105621-543471e840be // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20191007204434-a023cd5227bd // indirect
	google.golang.org/grpc v1.24.0
)
