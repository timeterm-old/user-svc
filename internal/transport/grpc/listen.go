package grpctx

import (
	"net"

	"gitlab.com/timeterm/env"
	pb "gitlab.com/timeterm/proto/go/user"
	"gitlab.com/timeterm/user-svc/internal/service"
	"google.golang.org/grpc"
)

const envListenAddr = "LISTEN_ADDR"

func init() {
	env.RegisterSpec(ListenEnvSpec())
}

// Listen creates a new TCP listener at LISTEN_ADDR, listening for gRPC
// client connections.
func Listen(b *service.Base) error {
	envs, err := ListenEnvSpec().Resolve()
	if err != nil {
		return err
	}

	lis, err := net.Listen("tcp", envs[envListenAddr])
	if err != nil {
		return err
	}

	grpcServer := grpc.NewServer()
	pb.RegisterUserServer(grpcServer, NewUserServer(b))

	return grpcServer.Serve(lis)
}

// ListenEnvSpec specifies the required environment variables for calling
// Listen. Listen resolves these itself.
func ListenEnvSpec() env.Spec {
	return env.Spec{
		env.Var{
			Name:  envListenAddr,
			Value: "localhost:12345",
		},
	}
}
