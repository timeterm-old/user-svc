package grpctx

import (
	"context"
	"errors"

	"github.com/gogo/protobuf/types"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/timeterm/user-svc/internal/database/daos"

	pb "gitlab.com/timeterm/proto/go/user"
)

func (s UserServer) GetUserCode(ctx context.Context, req *pb.UserCodeRequest) (*pb.UserCodeResponse, error) {
	dao, ok := daos.GetUserCodeDAO(s.base.Reg)
	if !ok {
		log.Error().Msg("could not get user dao")
		return nil, errDBConn
	}

	userCode, err := dao.Get(req.GetCardId())
	if err != nil {
		if errors.Is(err, daos.ErrDoesNotExist) {
			return nil, status.Error(codes.NotFound, "could not find student code")
		}
		log.Error().Err(err).Msg("could not get user code")
		return nil, errDBConn
	}

	return &pb.UserCodeResponse{UserCode: userCode}, nil
}

func (s UserServer) CreateUserCode(ctx context.Context, req *pb.CreateUserCodeRequest) (*types.Empty, error) {
	dao, ok := daos.GetUserCodeDAO(s.base.Reg)
	if !ok {
		log.Error().Msg("could not get user code dao")
		return nil, errDBConn
	}

	_, err := dao.Get(req.GetCardId())
	switch {
	case errors.Is(err, daos.ErrDoesNotExist):
	case err == nil:
		return nil, status.Error(codes.AlreadyExists, "user code already exists")
	default:
		return nil, errDBConn
	}

	err = dao.Set(req.GetCardId(), req.GetUserCode())
	if err != nil {
		return nil, errDBConn
	}

	return new(types.Empty), nil
}
