package grpctx

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var errDBConn = status.Error(codes.Unavailable, "could not contact database")
