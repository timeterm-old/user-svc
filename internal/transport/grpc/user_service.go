package grpctx

import (
	"gitlab.com/timeterm/user-svc/internal/service"
)

type UserServer struct {
	base *service.Base
}

func NewUserServer(b *service.Base) UserServer {
	return UserServer{base: b}
}
