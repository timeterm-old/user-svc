package grpctx

import (
	"context"
	"errors"

	"github.com/gogo/protobuf/types"

	"github.com/rs/zerolog/log"
	"gitlab.com/timeterm/user-svc/internal/database/daos"
	"gitlab.com/timeterm/user-svc/internal/database/model"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/timeterm/proto/go/user"
)

func (s UserServer) NewSession(ctx context.Context, req *pb.SessionRequest) (*pb.SessionResponse, error) {
	dao, ok := daos.GetSessionDAO(s.base.Reg)
	if !ok {
		log.Error().Msg("could not get session dao")
		return nil, errDBConn
	}

	userDao, ok := daos.GetUserCodeDAO(s.base.Reg)
	if !ok {
		log.Error().Msg("could not get student card dao")
		return nil, errDBConn
	}

	_, err := userDao.Get(req.GetCardId())
	if err != nil {
		return nil, status.Error(codes.NotFound, "could not find user")
	}

	sessID, err := dao.Create(model.Session{
		Type:   model.StUser,
		CardID: req.GetCardId(),
	})
	if err != nil {
		return nil, status.Error(codes.NotFound, "could not create session")
	}

	return &pb.SessionResponse{
		Session: &pb.Session{
			Id:        sessID,
			CardId:    req.GetCardId(),
			ExpiresIn: types.DurationProto(dao.Timeout),
		},
	}, nil
}

func (s UserServer) ValidateSession(ctx context.Context,
	req *pb.ValidateSessionRequest,
) (*pb.ValidateSessionResponse, error) {
	dao, ok := daos.GetSessionDAO(s.base.Reg)
	if !ok {
		log.Error().Msg("could not get session dao")
		return nil, errDBConn
	}

	sess, err := dao.Get(req.GetSessionId())
	if err != nil {
		if errors.Is(err, daos.ErrDoesNotExist) {
			return &pb.ValidateSessionResponse{Valid: false}, nil
		}
		return nil, errDBConn
	}

	ttl, err := dao.TTL(req.GetSessionId())
	if err != nil {
		return nil, errDBConn
	}

	return &pb.ValidateSessionResponse{
		Valid: true,
		Session: &pb.Session{
			Id:        req.GetSessionId(),
			CardId:    sess.CardID,
			ExpiresIn: types.DurationProto(ttl),
		},
	}, nil
}
