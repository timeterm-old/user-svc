package service

import (
	"fmt"
	"os"

	"github.com/opentracing/opentracing-go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/timeterm/env"
	"gitlab.com/timeterm/user-svc/internal/database"
	"gitlab.com/timeterm/user-svc/internal/database/daos"
)

const envModeDev = "MODE_DEV"

func init() {
	env.RegisterSpec(NewBaseEnvSpec())
}

// Base defines the base of the service. For now it only contains a database and DAO registry.
type Base struct {
	DB  *database.Database
	Reg daos.Registry
}

// NewBase creates a new service base containing a database and DAO registry.
// It also initializes a tracer and logger at the global level, and checks if all required environment
// variables are resolved as to satisfaction. Base.Close should be called at shutdown of owner.
func NewBase() (Base, error) {
	var base Base

	opentracing.InitGlobalTracer(opentracing.NoopTracer{})

	log.Logger = log.With().Caller().Logger()

	devEnvs, _ := NewBaseEnvSpec().Resolve()
	if devEnvs[envModeDev] == "true" {
		log.Logger = log.Logger.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	_, err := env.GlobalSpec().Resolve()
	if err != nil {
		return base, err
	}

	db, err := database.Open()
	if err != nil {
		return base, fmt.Errorf("could not open database: %w", err)
	}

	reg, err := daos.NewRegistry(db)
	if err != nil {
		return base, fmt.Errorf("could not create DAO registry: %w", err)
	}

	return Base{
		DB:  db,
		Reg: reg,
	}, nil
}

// Close closes the service (the contained database).
func (b *Base) Close() error {
	return b.DB.Close()
}

func NewBaseEnvSpec() env.Spec {
	return env.Spec{
		{
			Name:     envModeDev,
			Value:    "false",
			Help:     "To act in development mode or not",
			Required: false,
		},
	}
}
