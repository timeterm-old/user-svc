package model

import (
	"errors"

	"github.com/francoispqt/gojay"
)

type SessionType string

const (
	StUser SessionType = "user"
)

func (s SessionType) Valid() bool {
	switch s {
	case StUser:
	default:
		return false
	}
	return true
}

type Session struct {
	Type   SessionType
	CardID uint32
}

func (s *Session) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	switch key {
	case "type":
		err := dec.String((*string)(&s.Type))
		if err != nil {
			return err
		}

		if !s.Type.Valid() {
			return errors.New("invalid value for `type`")
		}
		return nil
	case "userId":
		return dec.Uint32(&s.CardID)
	}
	return nil
}

func (s *Session) NKeys() int {
	return 2
}

func (s *Session) MarshalJSONObject(enc *gojay.Encoder) {
	enc.StringKey("type", string(s.Type))
	enc.Uint32Key("userId", s.CardID)
}

func (s *Session) IsNil() bool {
	return s == nil
}
