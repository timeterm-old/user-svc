package database

import (
	"encoding"
	"fmt"

	"github.com/go-redis/redis"
)

// Database is a wrapper around `redis.Client` providing utilities for using hashes
// with UnmarshalText and MarshalText.
type Database struct {
	*redis.Client
}

// HGetUnmarshal gets the field field of some Redis hash `key` and unmarshals it into `v`.
// For more information, see https://redis.io/commands/hget.
func (db *Database) HGetUnmarshal(key, field string, v encoding.TextUnmarshaler) error {
	val, err := db.HGet(key, field).Result()
	if err != nil {
		return fmt.Errorf("could not get hash from Redis: %w", err)
	}

	err = v.UnmarshalText([]byte(val))
	if err != nil {
		return fmt.Errorf("could not unmarshal v: %w", err)
	}
	return err
}

// HSetMarshal sets the field of some Redis hash `key` to the marshaled value of `v`.
// For more information, see https://redis.io/commands/hset.
func (db *Database) HSetMarshal(key, field string, v encoding.TextMarshaler) error {
	val, err := v.MarshalText()
	if err != nil {
		return fmt.Errorf("could not marshal v: %w", err)
	}

	err = db.HSet(key, field, string(val)).Err()
	if err != nil {
		return fmt.Errorf("could not set hash value in Redis: %w", err)
	}
	return err
}
