package daos

import "gitlab.com/timeterm/user-svc/internal/database"

// DAO represents a data access object.
type DAO interface {
	Collection() string
	Init(db *database.Database) error
}
