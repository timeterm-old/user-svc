package daos

import (
	"bytes"
	"fmt"
	"time"

	"github.com/francoispqt/gojay"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/timeterm/env"
	"gitlab.com/timeterm/user-svc/internal/database"
	"gitlab.com/timeterm/user-svc/internal/database/model"
)

func init() {
	env.RegisterSpec(InitSessionDAOEnvSpec())
}

const (
	collectionSession = "user-svc_session"

	envSessionTimeout = "SESSION_TIMEOUT"
)

type SessionDAO struct {
	db      *database.Database
	Timeout time.Duration
}

func GetSessionDAO(r Registry) (s *SessionDAO, ok bool) {
	dao, exists := r.GetDAO(collectionSession)
	if !exists {
		return nil, false
	}

	sessionDAO, ok := dao.(*SessionDAO)
	return sessionDAO, ok
}

func (s *SessionDAO) Collection() string {
	return collectionSession
}

func (s *SessionDAO) Init(db *database.Database) error {
	envs, err := InitSessionDAOEnvSpec().Resolve()
	if err != nil {
		return err
	}

	timeout, err := time.ParseDuration(envs[envSessionTimeout])
	if err != nil {
		return err
	}

	s.db = db
	s.Timeout = timeout

	return nil
}

func (s *SessionDAO) newKey(id string) string {
	var buf bytes.Buffer

	buf.WriteString(s.Collection())
	buf.WriteRune(':')
	buf.WriteString(id)

	return buf.String()
}

func (s *SessionDAO) Create(sess model.Session) (id string, err error) {
	uid, err := uuid.NewV4()
	if err != nil {
		return "", fmt.Errorf("could not generate session id: %w", err)
	}

	id = uid.String()
	key := s.newKey(id)

	value, err := gojay.Marshal(&sess)
	if err != nil {
		return "", err
	}

	return id, s.db.Set(key, value, s.Timeout).Err()
}

func (s *SessionDAO) Get(id string) (sess model.Session, err error) {
	key := s.newKey(id)

	exists, err := s.db.Exists(key).Result()
	if err != nil {
		return model.Session{}, err
	}
	if exists != 1 {
		return model.Session{}, ErrDoesNotExist
	}

	sessStr, err := s.db.Get(key).Result()
	if err != nil {
		return model.Session{}, err
	}

	err = gojay.Unmarshal([]byte(sessStr), &sess)
	if err != nil {
		return model.Session{}, err
	}
	return sess, nil
}

func (s *SessionDAO) TTL(id string) (time.Duration, error) {
	key := s.newKey(id)

	ttl, err := s.db.PTTL(key).Result()
	if err != nil {
		return 0, err
	}
	return ttl, nil
}

func InitSessionDAOEnvSpec() env.Spec {
	return env.Spec{
		{
			Name:  envSessionTimeout,
			Help:  "The amount of time a session lasts",
			Value: "3600s",
		},
	}
}
