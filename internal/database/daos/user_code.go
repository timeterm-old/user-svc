package daos

import (
	"strconv"

	"gitlab.com/timeterm/user-svc/internal/database"
)

const collectionUserCode = "user-svc_user_code"

type UserCodeDAO struct {
	db *database.Database
}

// GetUserCodeDAO from Registry r. If the DAO does not exist, or the "user_code"
// collection is not associated with a UserCodeDAO,  false is returned. Only
// assert that the UserCodeDAO is valid if ok is true.
func GetUserCodeDAO(r Registry) (s *UserCodeDAO, ok bool) {
	dao, exists := r.GetDAO(collectionUserCode)
	if !exists {
		return nil, false
	}

	userDAO, ok := dao.(*UserCodeDAO)
	return userDAO, ok
}

// Init initializes the UserCodeDAO by setting the database to read and write from.
func (u *UserCodeDAO) Init(db *database.Database) error {
	u.db = db
	return nil
}

// Collection returns the (hash)map to store the values of the UserCodeDAO in.
func (u *UserCodeDAO) Collection() string {
	return collectionUserCode
}

// Get retrieves a user by its identifier. This is usually a student number or a teacher code.
func (u *UserCodeDAO) Get(cardID uint32) (string, error) {
	idStr := strconv.FormatUint(uint64(cardID), 16)
	exists, err := u.db.HExists(u.Collection(), idStr).Result()
	if err != nil {
		return "", err
	}
	if !exists {
		return "", ErrDoesNotExist
	}

	var userCode string
	userCode, err = u.db.HGet(u.Collection(), idStr).Result()
	if err != nil {
		return "", err
	}

	return userCode, nil
}

// Set sets the preferences of a user.
func (u *UserCodeDAO) Set(cardID uint32, userCode string) error {
	idStr := strconv.FormatUint(uint64(cardID), 16)
	return u.db.HSet(u.Collection(), idStr, userCode).Err()
}
