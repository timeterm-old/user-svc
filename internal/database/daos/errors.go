package daos

const ErrDoesNotExist errorMessage = "does not exist"

type errorMessage string

func (e errorMessage) Error() string { return string(e) }
