package database

type String string

func (s *String) UnmarshalText(text []byte) error {
	*s = String(text)
	return nil
}

func (s *String) MarshalText() ([]byte, error) {
	return []byte(*s), nil
}
