GOARCH ?= amd64

.PHONY: ci-setup
ci-setup: 
	git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/".insteadOf "https://gitlab.com/"
	mkdir -p ${CI_PROJECT_DIR}/.cache
	export GOPATH=${CI_PROJECT_DIR}/.cache

.PHONY: build-user-svc-linux
build-user-svc-linux:
	GOOS=linux $(MAKE) build-user-svc

.PHONY: build-user-svc
build-user-svc:
	mkdir -p build
	CGO_ENABLED=0 installsuffix=cgo go build -ldflags "-extldflags '-static'" -o ./cmd/user-svc/user-svc-$(GOOS)-$(GOARCH) ./cmd/user-svc
	strip --strip-all ./cmd/user-svc/user-svc-$(GOOS)-$(GOARCH)

.PHONY: docker-user-svc-linux
docker-user-svc-linux: build-user-svc-linux
	docker build cmd/user-svc
