package app

import (
	"flag"
	"os"
	"os/signal"

	"gitlab.com/timeterm/env"

	"github.com/rs/zerolog/log"
	"gitlab.com/timeterm/user-svc/internal/service"
	grpctx "gitlab.com/timeterm/user-svc/internal/transport/grpc"
)

var flagEnvs = flag.Bool("envs", false, "Print all settable environment variables")

func Execute() {
	flag.Parse()

	if *flagEnvs {
		env.PrintGlobalSpec()
		return
	}

	b, err := service.NewBase()
	if err != nil {
		log.Fatal().Err(err).Msg("Could not initialize service")
		os.Exit(1)
	}

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		// Block till a signal is received.
		s := <-c
		log.Info().Str("signal", s.String()).Msg("Received signal")
		log.Info().Msg("Closing service")

		err := b.Close()
		if err != nil {
			log.Fatal().Err(err).Msg("Could not close service")
		}

		os.Exit(0)
	}()

	// Basically, this should never return.
	err = grpctx.Listen(&b)
	log.Fatal().Err(err).Msg("Error listening")
}
