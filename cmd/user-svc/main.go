package main

import (
	"gitlab.com/timeterm/user-svc/cmd/user-svc/app"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	app.Execute()
}
